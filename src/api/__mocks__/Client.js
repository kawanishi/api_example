import axios from "axios";
import MockAdapter from "axios-mock-adapter";

const Client = axios.create({
  baseURL: "https://stg.enicia.me/api/v1",
  headers: { "Content-Type": "application/json" }
});

// DEBUG: print request headers
// Client.interceptors.request.use(request => {
//   console.log(request.headers);
//   return request;
// });

const mock = new MockAdapter(Client);

mock
  .onPost("/users/sign_in", {
    email: "testSucceed",
    password: "testSucceed"
  })
  .reply(200, {}, { authorization: "Bearer testSucceed" })
  .onPost("/users/sign_in", {
    email: "testFail",
    password: "testFail"
  })
  .reply(401, { message: "会員情報が存在しません" });

mock
  .onGet(
    "/me",
    {},
    {
      ...Client.defaults.headers.common,
      "Content-Type": "application/json",
      Authorization: "Bearer testSucceed"
    }
  )
  .reply(200, {
    name: "testName",
    real_world_name: "testRealWorldName",
    display_name: "testDisplayName",
    email: "test@test",
    tel: "0009990000",
    birthday: "2000-01-01",
    address: {
      zip_code: "111-0000",
      prefecture: "テスト県",
      city: "テスト市",
      address: "テストアドレス1-1"
    },
    avatar: {
      large: "testLarge.jpeg",
      thumb: "testThumb.jpeg"
    },
    allow_push_notification: true,
    main_shop: {
      id: 1,
      name: "TEST店"
    },
    main_role: {
      id: 2,
      name: "TESTCREW",
      alias: "testing"
    },
    bio: "テストしています。",
    abilities: [
      {
        id: 5,
        name: "example"
      }
    ]
  })
  .onGet(
    "/me",
    {},
    {
      ...Client.defaults.headers.common,
      "Content-Type": "application/json",
      Authorization: "Bearer updateSucceed"
    }
  )
  .reply(200, {
    name: "testName",
    real_world_name: "testRealWorldName",
    display_name: "updateDisplayName",
    email: "test@test",
    tel: "1119991111",
    birthday: "2010-01-01",
    address: {
      zip_code: "111-1111",
      prefecture: "更新県",
      city: "更新市",
      address: "テスト更新1-1"
    },
    avatar: {
      large: "updateLarge.jpeg",
      thumb: "updateThumb.jpeg"
    },
    allow_push_notification: false,
    main_shop: {
      id: 3,
      name: "更新店"
    },
    main_role: {
      id: 6,
      name: "UPDATINGCREW",
      alias: "updating"
    },
    bio: "更新テスト中",
    abilities: [{ id: 10, name: "example_1" }, { id: 20, name: "example_2" }]
  })
  .onGet(
    "/me",
    {},
    {
      ...Client.defaults.headers.common,
      "Content-Type": "application/json",
      Authorization: "Bearer tokenExpired"
    }
  )
  .reply(401, {
    error: "アカウント登録もしくはログインしてください。"
  })
  .onGet("/shops", {
    params: {
      "location[latitude]": 42.0,
      "location[longitude]": 42.0,
      radius: 10
    }
  })
  .replyOnce(200, [
    {
      id: 1,
      name: "example_1",
      alias_name: "example_1",
      zip_code: "111-1111",
      address: "ショップリスト1-1",
      tel: "111-111-1111",
      photo: {
        large: "shoplistlarge_1.jpeg",
        thumb: "shoplistthumb_1.jpeg"
      },
      hours_from: "6:00",
      hours_to: "18:00",
      location: {
        latitude: 42.0,
        longitude: 42.0
      },
      capacity: 1,
      people_count: 1,
      recently_people: [
        {
          id: 1,
          name: "people_1",
          display_name: "people_1",
          bio: "people_1",
          is_following: true,
          last_checked_in_at: "2015-01-01T12:00:00Z",
          last_checked_in_at_in_words: "example_1",
          avatar: {
            large: "peoplelarge_1.jpeg",
            thumb: "peoplethumb_1.jpeg"
          }
        }
      ],
      amenities: [
        {
          id: 1,
          name: "amenity_1",
          display_name: "amenity_1"
        }
      ]
    },
    {
      id: 2,
      name: "example_2",
      alias_name: "example_2",
      zip_code: "222-2222",
      address: "ショップリスト2-2",
      tel: "222-222-2222",
      photo: {
        large: "shoplistlarge_2.jpeg",
        thumb: "shoplistthumb_2.jpeg"
      },
      hours_from: "5:00",
      hours_to: "22:00",
      location: {
        latitude: 42.5,
        longitude: 42.5
      },
      capacity: 2,
      people_count: 2,
      recently_people: [
        {
          id: 2,
          name: "people_2",
          display_name: "people_2",
          bio: "people_2",
          is_following: false,
          last_checked_in_at: "2018-01-01T12:00:00Z",
          last_checked_in_at_in_words: "example_2",
          avatar: {
            large: "peoplelarge_2.jpeg",
            thumb: "peoplethumb_2.jpeg"
          }
        }
      ],
      amenities: [
        {
          id: 2,
          name: "amenity_2",
          display_name: "amenity_2"
        }
      ]
    }
  ])
  .onGet("/shops", {
    params: {
      "location[latitude]": 100,
      "location[longitude]": 100,
      radius: 10
    }
  })
  .replyOnce(200, [
    {
      id: 4,
      name: "example_4",
      alias_name: "example_4",
      zip_code: "444-4444",
      address: "ショップリスト4-4",
      tel: "444-4444",
      photo: {
        large: "shoplistlarge_4.jpeg",
        thumb: "shoplistthumb_4.jpeg"
      },
      hours_from: "6:00",
      hours_to: "18:00",
      location: {
        latitude: 100,
        longitude: 100
      },
      capacity: 4,
      people_count: 4,
      recently_people: [
        {
          id: 4,
          name: "people_4",
          display_name: "people_4",
          bio: "people_4",
          is_following: true,
          last_checked_in_at: "2015-04-01T12:00:00Z",
          last_checked_in_at_in_words: "example_4",
          avatar: {
            large: "peoplelarge_4.jpeg",
            thumb: "peoplethumb_4.jpeg"
          }
        }
      ],
      amenities: [
        {
          id: 1,
          name: "amenity_4",
          display_name: "amenity_4"
        }
      ]
    },
    {
      id: 5,
      name: "example_5",
      alias_name: "example_5",
      zip_code: "555-5555",
      address: "ショップリスト5-5",
      tel: "555-555-5555",
      photo: {
        large: "shoplistlarge_5.jpeg",
        thumb: "shoplistthumb_5.jpeg"
      },
      hours_from: "5:00",
      hours_to: "22:00",
      location: {
        latitude: 100.5,
        longitude: 100.5
      },
      capacity: 5,
      people_count: 5,
      recently_people: [
        {
          id: 5,
          name: "people_5",
          display_name: "people_5",
          bio: "people_5",
          is_following: false,
          last_checked_in_at: "2018-05-01T12:00:00Z",
          last_checked_in_at_in_words: "example_5",
          avatar: {
            large: "peoplelarge_5.jpeg",
            thumb: "peoplethumb_5.jpeg"
          }
        }
      ],
      amenities: [
        {
          id: 5,
          name: "amenity_5",
          display_name: "amenity_5"
        }
      ]
    }
  ])
  .onGet("/shops/digests")
  .replyOnce(200, [{ id: 1, name: "example_1" }, { id: 2, name: "example_2" }])
  .onGet("/roles")
  .replyOnce(200, [
    { id: 1, name: "example_1", alias: "example_1" },
    { id: 2, name: "example_2", alias: "example_2" }
  ])
  .onGet("/abilities")
  .replyOnce(200, [{ id: 1, name: "example_1" }, { id: 2, name: "example_2" }])
  .onGet("/shops/1")
  .replyOnce(200, {
    id: 1,
    name: "SHOPLISTテスト",
    zip_code: "999-9999",
    address: "テストテストtest1-2-3",
    tel: "999-888-7777",
    photo: {
      large: "shoptestlarge.jpeg",
      thumb: "shoptestthumb.jpeg"
    },
    hours_from: "9:00",
    hours_to: "13:00",
    location: {
      latitude: 42.0,
      longitude: 42.0
    },
    capacity: 42,
    people_count: 42,
    recently_people: [
      {
        id: 42,
        name: "example",
        display_name: "example",
        bio: "example",
        is_following: true,
        last_checked_in_at: "2015-01-01T12:00:00Z",
        last_checked_in_at_in_words: "example",
        avatar: {
          large: "example",
          thumb: "example"
        }
      }
    ],
    amenities: [
      {
        id: 42,
        name: "example",
        display_name: "example"
      }
    ]
  })
  .onGet(
    "/shops/3/check_in_users",
    {},
    {
      ...Client.defaults.headers.common,
      "Content-Type": "application/json",
      Authorization: "Bearer checkInUsers"
    }
  )
  .replyOnce(200, {
    shop: {
      id: 3,
      name: "example_3"
    },
    check_in_users: [
      {
        id: 42,
        name: "example",
        display_name: "example",
        bio: "example",
        is_following: true,
        last_checked_in_at: "2015-01-01T12:00:00Z",
        last_checked_in_at_in_words: "example",
        avatar: {
          large: "example",
          thumb: "example"
        }
      }
    ]
  })
  .onGet(
    "/me",
    {},
    {
      ...Client.defaults.headers.common,
      "Content-Type": "application/json",
      Authorization: "Bearer testFail"
    }
  )
  .networkError();

const updateData = {
  display_name: "updateDisplayName",
  tel: "1119991111",
  birthday: "2010-01-01",
  address: {
    zip_code: "111-1111",
    prefecture: "更新県",
    city: "更新市",
    address: "テスト更新1-1"
  },
  allow_push_notification: false,
  bio: "更新テスト中",
  main_shop_id: 3,
  main_role_id: 6,
  ability_ids: [10, 20],
  avatar: "data:;base64,YmxvYjp0ZXN0"
};

mock
  .onAny("/me/profiles", updateData, {
    ...Client.defaults.headers.common,
    "Content-Type": "application/json",
    Authorization: "Bearer updateSucceed"
  })
  .reply(200)
  .onAny("/me/profiles", updateData, {
    ...Client.defaults.headers.common,
    "Content-Type": "application/json",
    Authorization: "Bearer updateFail"
  })
  .networkError();

export default Client;
