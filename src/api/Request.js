import humps from "humps";
import Client from "./Client";

export const postSignIn = data => Client.post("/users/sign_in", data);

export const getMeInfo = auth => {
  const responseData = Client.get("/me", { headers: { Authorization: auth } }).then(response => {
    return humps.camelizeKeys(response.data);
  });
  return responseData;
};

export const patchMeProfiles = (auth, data) =>
  Client.patch("/me/profiles", data, { headers: { Authorization: auth } });

export const getShopsDigests = () => Client.get("/shops/digests");

export const getRoles = () => Client.get("/roles");

export const getAbilities = () => Client.get("/abilities");

export const getShopsInfo = shopId => {
  const responseData = Client.get(`/shops/${shopId}`).then(response => {
    return humps.camelizeKeys(response.data);
  });
  return responseData;
};

export const getShopsList = location => {
  const responseData = Client.get("/shops", {
    params: {
      "location[latitude]": location.latitude,
      "location[longitude]": location.longitude,
      radius: 10
    }
  }).then(response => {
    return humps.camelizeKeys(response.data);
  });
  return responseData;
};

export const getShopCheckInUsers = (auth, shopId) => {
  const responseData = Client.get(`/shops/${shopId}/check_in_users`, { headers: { Authorization: auth } }).then(response => {
    return humps.camelizeKeys(response.data);
  });
  return responseData;
};
