import axios from "axios";
// Disable CORS on browser temporarily
// open /Applications/Vivaldi.app/ --args --disable-web-security --user-data-dir

const Client = axios.create({
  baseURL: "https://stg.enicia.me/api/v1",
  headers: { "Content-Type": "application/json" }
});

export default Client;
