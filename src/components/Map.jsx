import React from "react";
import { reaction } from "mobx";
import { inject, observer } from "mobx-react";
import PropTypes from "prop-types";
import { compose, withProps, withState, withHandlers, withStateHandlers } from "recompose";
import { withScriptjs, withGoogleMap, GoogleMap, Marker, InfoWindow } from "react-google-maps";

const ShopsMapComponent = compose(
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?key=" + process.env.GOOGLE_API_KEY + "&v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `85vh` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withState("zoom", "onZoomChange", 14),
  withHandlers(() => {
    const refs = {
      map: undefined,
    }
    return {
      onMapMounted: () => ref => {
        refs.map = ref
      },
      onZoomChanged: ({ onZoomChange }) => () => {
        onZoomChange(refs.map.getZoom());
      },
    }
  }),
  withStateHandlers(() => ({
    selectedShopId: 0,
  }), {
    onMarkerClick: ({ selectedShopId }) => shop => ({
      selectedShopId: shop.id,
    })
  }),
  withScriptjs,
  withGoogleMap
)(props =>
  <GoogleMap
    center={props.center}
    zoom={props.zoom}
    ref={props.onMapMounted}
    onZoomChanged={props.onZoomChanged}
  >
    {props.shops.map(shop =>
      <Marker
        position={{ lat: shop.location.latitude, lng: shop.location.longitude }}
        onClick={() => {
          props.onMarkerClick(shop);
          props.showCheckInUsers(shop.id);
        }}
      >
        {props.selectedShopId === shop.id &&
        <InfoWindow onCloseClick={props.onToggleOpen}>
          <div style={{ width:"200px" }}>
            <div><strong>{shop.name}</strong></div>
            <div><img src={shop.photo.thumb} alt={shop.name} style={{ width:"100%" }} /></div>
            <div>{shop.zipCode}</div>
            <div>{shop.address}</div>
            <div>電話番号：{shop.tel}</div>
            <div>営業時間：{shop.hoursFrom} 〜 {shop.hoursTo}</div>
            <ul>アメニティー{shop.amenities.map(amenity =>
              <li>{amenity.displayName}</li>
            )}</ul>
            <p>チェックインユーザー</p>
            {props.checkInUsers.checkInUsers &&
              props.checkInUsers.checkInUsers.map(user =>
                <div>
                  <div>
                    {user.avatar.thumb && <img src={user.avatar.thumb} alt={user.name} style={{ width:"20%", marginRight:"10px" }} />}
                    <span>{user.displayName}</span>
                  </div>
                </div>
              )
            }
          </div>
        </InfoWindow>}
      </Marker>
    )}
  </GoogleMap>
);

@inject("shop")
@inject("user")
@observer
export default class Map extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      center: {
        lat: 35.1674,
        lng: 136.9
      },
      showShopId: 0
    }
  }

  showCheckInUsers(shopId) {
    this.props.shop.getCheckInUsers(this.props.user.authorization, shopId);
  }

  handleClickCurrentLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        this.setState({
          center: {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          }
        });
        this.props.shop.getShopListAroundCurrentLocation({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude
        });
      });
    } else {
      alert("ご利用のブラウザでは現在地を取得できません。");
    }
  }

  handleChangeShop(event) {
    this.setState({showShopId: event.target.value});
    this.props.shop.getShopInfo(event.target.value);
  }

  componentDidMount() {
    this.props.shop.getShopDigests();
    this.props.shop.getShopInfo(this.props.user.me.mainShop.id);
    this.setState({showShopId: this.props.user.me.mainShop.id});
  }

  componentDidUpdate() {
    reaction(
      () => this.props.shop.selectedShop.location.latitude,
      () => {
        this.setState({
          center: {
            lat: this.props.shop.selectedShop.location.latitude,
            lng: this.props.shop.selectedShop.location.longitude
          }
        });
      }
    );
  }

  render() {
    const mainShopName = this.props.user.me.mainShop.name;
    const shopOptions = this.props.shop.shopDigests.map(shop => (
      <option key={shop.id} value={shop.id} >{shop.name}</option>
    ));
    const shops = this.props.shop.shopList.slice();
    const checkInUsers = this.props.shop.checkInUsers;
    return(
      <div>
        <div>
          <strong>店舗情報</strong>
          {this.props.shop.shopInfoStatus.hasError && <strong style={{color:"red", marginLeft:"10px"}}>{this.props.shop.shopInfoStatus.errorMessage}</strong>}
        </div>
        <div><span>主に利用する店舗：{mainShopName}</span></div>
        <div style={{ marginBottom:"10px" }}>
          <button onClick={this.handleClickCurrentLocation.bind(this)} style={{ marginRight:"10px" }}>現在地周辺の店舗</button>
          <select name="mainShopId" key={this.state.showShopId} value={this.state.showShopId} onChange={this.handleChangeShop.bind(this)}>{shopOptions}</select>
        </div>
        <ShopsMapComponent
          shops={shops}
          checkInUsers={checkInUsers}
          showCheckInUsers={this.showCheckInUsers.bind(this)}
          center={this.state.center}
        />
      </div>
    );
  }
}

Map.wrappedComponent.propTypes = {
  shop: PropTypes.shape({
    selectedShop: PropTypes.shape({
      address: PropTypes.string.isRequired,
      amenities: PropTypes.objectOf(PropTypes.array).isRequired,
      capacity: PropTypes.number.isRequired,
      hoursFrom: PropTypes.string.isRequired,
      hoursTo: PropTypes.string.isRequired,
      id: PropTypes.number.isRequired,
      location: PropTypes.shape({
        latitude: PropTypes.number.isRequired,
        longitude: PropTypes.number.isRequired
      }).isRequired,
      name: PropTypes.string.isRequired,
      peopleCount: PropTypes.number.isRequired,
      photo: PropTypes.shape({
        large: PropTypes.string.isRequired,
        thumb: PropTypes.string.isRequired
      }).isRequired,
      recentlyPeople: PropTypes.objectOf(PropTypes.array).isRequired,
      tel: PropTypes.string.isRequired,
      zipCode: PropTypes.string.isRequired
    }).isRequired,
    shopDigests: PropTypes.objectOf(PropTypes.array).isRequired,
    shopList: PropTypes.objectOf(PropTypes.array).isRequired,
    checkInUsers: PropTypes.object.isRequired,
    shopInfoStatus: PropTypes.shape({
      hasError: PropTypes.bool.isRequired,
      errorMessage: PropTypes.string.isRequired
    }).isRequired,
    getShopDigests: PropTypes.func.isRequired,
    getShopInfo: PropTypes.func.isRequired,
    getShopList: PropTypes.func.isRequired,
    getCheckInUsers: PropTypes.func.isRequired,
    getShopListAroundCurrentLocation: PropTypes.func.isRequired
  }).isRequired,
  user: PropTypes.shape({
    authorization: PropTypes.string.isRequired,
    me: PropTypes.shape({
      mainShop: PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired
      }).isRequired,
    }).isRequired,
  }).isRequired
}
