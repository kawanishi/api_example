import React from "react";
import { inject, observer } from "mobx-react";
import PropTypes from "prop-types";

@inject("user")
@observer
export default class Home extends React.Component {
  constructor(props) {
    super(props);
    this.handleClickEdit = this.handleClickEdit.bind(this);
    this.handleClickShop = this.handleClickShop.bind(this);
  }

  handleClickEdit(event) {
    event.preventDefault();
    this.props.history.push("/edit");
  }

  handleClickShop(event) {
    event.preventDefault();
    this.props.history.push("/map");
  }

  componentDidUpdate() {
    if (this.props.user.loginStatus.hasError) {
      alert(this.props.user.loginStatus.errorMessage);
    } else if (this.props.user.editStatus.hasError) {
      alert(this.props.user.editStatus.errorMessage);
    }
  }

  render() {
    const { user } = this.props;
    return (
      <div>
        <strong>USER INFORMATION</strong>
        &nbsp;
        <button onClick={this.handleClickEdit}>プロフィール編集</button>
        &nbsp;
        <button onClick={this.handleClickShop}>店舗情報</button>
        {user.me.avatar.thumb && <div><img src={user.me.avatar.thumb} alt="自画像" style={{margin:"10px"}} /></div>}
        <p>ユーザーID：{user.me.name}</p>
        <p>本名：{user.me.realWorldName}</p>
        <p>SNS表示名：{user.me.displayName}</p>
        <p>メールアドレス：{user.me.email}</p>
        <p>電話番号：{user.me.tel}</p>
        <p>生年月日：{user.me.birthday}</p>
        <p>郵便番号：{user.me.address.zipCode}</p>
        <p>住所：{user.me.address.prefecture}{user.me.address.city}{user.me.address.address}</p>
        <p>PUSH通知設定：{(user.me.allowPushNotification && <span>受信する</span>) || <span>受信しない</span>}</p>
        <p>主に利用する店舗：{user.me.mainShop.name}</p>
        <p>メインの役割：{user.me.mainRole.name}</p>
        <p>自己紹介：{user.me.bio}</p>
        <div>アビリティ：<ul>{user.me.abilities.map(ability => (
          <li key={ability.id}>{ability.name}</li>
        ))}</ul></div>
        {user.loginStatus.hasError}
        {user.editStatus.hasError}
      </div>
    );
  }
}

Home.wrappedComponent.propTypes = {
  user: PropTypes.shape({
    me: PropTypes.shape({
      name: PropTypes.string.isRequired,
      realWorldName: PropTypes.string.isRequired,
      displayName: PropTypes.string.isRequired,
      email: PropTypes.string.isRequired,
      tel: PropTypes.string.isRequired,
      birthday: PropTypes.string.isRequired,
      address: PropTypes.shape({
        zipCode: PropTypes.string.isRequired,
        prefecture: PropTypes.string.isRequired,
        city: PropTypes.string.isRequired,
        address: PropTypes.string.isRequired
      }).isRequired,
      avatar: PropTypes.shape({
        thumb: PropTypes.string.isRequired
      }).isRequired,
      allowPushNotification: PropTypes.bool.isRequired,
      mainShop: PropTypes.shape({
        name: PropTypes.string.isRequired
      }).isRequired,
      mainRole: PropTypes.shape({
        name: PropTypes.string.isRequired
      }).isRequired,
      bio: PropTypes.string.isRequired,
      abilities: PropTypes.objectOf(PropTypes.array).isRequired
    }).isRequired
  }).isRequired
}
