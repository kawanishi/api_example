import React from "react";
import { Provider } from "mobx-react";
import { Route, Switch } from "react-router-dom";
import User from "./store/User.js";
import Shop from "./store/Shop.js";
import Login from "./components/Login.jsx";
import Home from "./components/Home.jsx";
import Edit from "./components/Edit.jsx";
import Map from "./components/Map.jsx";

const user = new User();
const shop = new Shop();

export default class App extends React.Component {
  render() {
    return (
      <Provider user={user} shop={shop}>
        <div>
          <Switch>
            <Route exact path="/" component={Login} />
            <Route path="/home" component={Home} />
            <Route path="/edit" component={Edit} />
            <Route path="/map" component={Map} />
          </Switch>
        </div>
      </Provider>
    )
  }
}
