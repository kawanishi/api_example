import * as mobx from "mobx";
import * as Request from "../api/Request";

const { observable, action, reaction } = mobx;

export default class Shop {
  constructor() {
    reaction(
      () => this.selectedShop.id,
      () => this.getShopList()
    );
  }

  @observable
  selectedShop = {
    address: "",
    amenities: [],
    capacity: 0,
    hoursFrom: "",
    hoursTo: "",
    id: 0,
    location: {
      latitude: 0,
      longitude: 0
    },
    name: "",
    peopleCount: 0,
    photo: {
      large: "",
      thumb: ""
    },
    recentlyPeople: [],
    tel: "",
    zipCode: ""
  };

  @observable shopDigests = [];
  @observable shopList = [];
  @observable checkInUsers = {};

  @observable
  shopInfoStatus = {
    hasError: false,
    errorMessage: ""
  };

  @action
  async getShopDigests() {
    try {
      const res = await Request.getShopsDigests();
      this.shopDigests = res.data;
      this.shopInfoStatus.hasError = false;
    } catch (error) {
      this.shopInfoStatus.errorMessage = error.message;
      this.shopInfoStatus.hasError = true;
    }
  }

  @action
  async getShopInfo(id) {
    try {
      this.selectedShop = await Request.getShopsInfo(id);
      this.shopInfoStatus.hasError = false;
    } catch (error) {
      this.shopInfoStatus.errorMessage = error.message;
      this.shopInfoStatus.hasError = true;
    }
  }

  @action
  async getShopList() {
    try {
      this.shopList = await Request.getShopsList(this.selectedShop.location);
      this.shopInfoStatus.hasError = false;
    } catch (error) {
      this.shopInfoStatus.errorMessage = error.message;
      this.shopInfoStatus.hasError = true;
    }
  }

  @action
  async getCheckInUsers(auth, shopId) {
    try {
      this.checkInUsers = await Request.getShopCheckInUsers(auth, shopId);
      this.shopInfoStatus.hasError = false;
    } catch (error) {
      this.shopInfoStatus.errorMessage = error.message;
      this.shopInfoStatus.hasError = true;
    }
  }

  @action
  async getShopListAroundCurrentLocation(position) {
    try {
      this.shopList = await Request.getShopsList(position);
      this.shopInfoStatus.hasError = false;
    } catch (error) {
      this.shopInfoStatus.errorMessage = error.message;
      this.shopInfoStatus.hasError = true;
    }
  }
}
