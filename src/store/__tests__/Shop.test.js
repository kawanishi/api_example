import * as mobx from "mobx";
import Shop from "../Shop";
import * as Request from "../../api/Request";
import { mock } from "../../api/__mocks__/Client";

jest.mock("../../api/Client");

describe("Shop", () => {
  let shop;
  const initialStoreValue = {
    selectedShop: {
      address: "",
      amenities: [],
      capacity: 0,
      hoursFrom: "",
      hoursTo: "",
      id: 0,
      location: {
        latitude: 0,
        longitude: 0
      },
      name: "",
      peopleCount: 0,
      photo: {
        large: "",
        thumb: ""
      },
      recentlyPeople: [],
      tel: "",
      zipCode: ""
    },
    shopDigests: [],
    shopList: [],
    checkInUsers: {},
    shopInfoStatus: {
      hasError: false,
      errorMessage: ""
    }
  };

  beforeEach(() => {
    shop = new Shop();
    jest.clearAllMocks();
    jest.resetAllMocks();
  });

  describe("mobx of reaction in constructor()", () => {
    test("getShopList() should run", () => {
      shop.getShopList = jest.fn();
      shop.selectedShop.id = 1;
      expect(shop.getShopList).toHaveBeenCalledTimes(1);
      shop.selectedShop.id = 2;
      expect(shop.getShopList).toHaveBeenCalledTimes(2);
    });

    test("getShopList() should not run", () => {
      shop.getShopList = jest.fn();
      shop.selectedShop.id = 0;
      expect(shop.getShopList).toHaveBeenCalledTimes(0);
    });
  });

  describe("selectedShop", () => {
    it("should be initial value", () => {
      expect(mobx.toJS(shop.selectedShop)).toEqual(initialStoreValue.selectedShop);
    });
  });

  describe("shopDigests", () => {
    it("should be initial value", () => {
      expect(mobx.toJS(shop.shopDigests)).toEqual(initialStoreValue.shopDigests);
    });
  });

  describe("shopList", () => {
    it("should be initial value", () => {
      expect(mobx.toJS(shop.shopList)).toEqual(initialStoreValue.shopList);
    });
  });

  describe("checkInUsers", () => {
    it("should be initial value", () => {
      expect(mobx.toJS(shop.checkInUsers)).toEqual(initialStoreValue.checkInUsers);
    });
  });

  describe("shopInfoStatus", () => {
    it("should be initial value", () => {
      expect(mobx.toJS(shop.shopInfoStatus)).toEqual(initialStoreValue.shopInfoStatus);
    });
  });

  describe("getShopList()", () => {
    test("getShopList should succeed", async () => {
      shop.selectedShop.location = {
        latitude: 42.0,
        longitude: 42.0
      };
      await shop.getShopList();
      const expected = {
        ...initialStoreValue,
        selectedShop: {
          ...initialStoreValue.selectedShop,
          location: {
            latitude: 42.0,
            longitude: 42.0
          }
        },
        shopList: [
          {
            id: 1,
            name: "example_1",
            aliasName: "example_1",
            zipCode: "111-1111",
            address: "ショップリスト1-1",
            tel: "111-111-1111",
            photo: {
              large: "shoplistlarge_1.jpeg",
              thumb: "shoplistthumb_1.jpeg"
            },
            hoursFrom: "6:00",
            hoursTo: "18:00",
            location: {
              latitude: 42.0,
              longitude: 42.0
            },
            capacity: 1,
            peopleCount: 1,
            recentlyPeople: [
              {
                id: 1,
                name: "people_1",
                displayName: "people_1",
                bio: "people_1",
                isFollowing: true,
                lastCheckedInAt: "2015-01-01T12:00:00Z",
                lastCheckedInAtInWords: "example_1",
                avatar: {
                  large: "peoplelarge_1.jpeg",
                  thumb: "peoplethumb_1.jpeg"
                }
              }
            ],
            amenities: [
              {
                id: 1,
                name: "amenity_1",
                displayName: "amenity_1"
              }
            ]
          },
          {
            id: 2,
            name: "example_2",
            aliasName: "example_2",
            zipCode: "222-2222",
            address: "ショップリスト2-2",
            tel: "222-222-2222",
            photo: {
              large: "shoplistlarge_2.jpeg",
              thumb: "shoplistthumb_2.jpeg"
            },
            hoursFrom: "5:00",
            hoursTo: "22:00",
            location: {
              latitude: 42.5,
              longitude: 42.5
            },
            capacity: 2,
            peopleCount: 2,
            recentlyPeople: [
              {
                id: 2,
                name: "people_2",
                displayName: "people_2",
                bio: "people_2",
                isFollowing: false,
                lastCheckedInAt: "2018-01-01T12:00:00Z",
                lastCheckedInAtInWords: "example_2",
                avatar: {
                  large: "peoplelarge_2.jpeg",
                  thumb: "peoplethumb_2.jpeg"
                }
              }
            ],
            amenities: [
              {
                id: 2,
                name: "amenity_2",
                displayName: "amenity_2"
              }
            ]
          }
        ],
        shopInfoStatus: {
          hasError: false,
          errorMessage: ""
        }
      }
      expect(mobx.toJS(shop)).toEqual(expected);
    });

    test("getShopList should fail", async () => {
      shop.selectedShop.location = {
        latitude: 42.0,
        longitude: 42.0
      };
      await shop.getShopList();
      expect(() => {
        Request.getShopsList(error.message);
      }).toThrow();
      const expected = {
        ...initialStoreValue,
        selectedShop: {
          ...initialStoreValue.selectedShop,
          location: {
            latitude: 42.0,
            longitude: 42.0
          }
        },
        shopInfoStatus: {
          hasError: true,
          errorMessage: "Request failed with status code 404"
        }
      }
      expect(mobx.toJS(shop)).toEqual(expected);
    });
  });

  describe("getShopDigests()", () => {
    test("getShopDigests should succeed", async () => {
      await shop.getShopDigests();
      const expected = {
        ...initialStoreValue,
        shopDigests: [
          { id: 1, name: "example_1" },
          { id: 2, name: "example_2" }
        ],
        shopInfoStatus: {
          hasError: false,
          errorMessage: ""
        }
      }
      expect(mobx.toJS(shop)).toEqual(expected);
    });

    test("getShopDigests should fail", async () => {
      await shop.getShopDigests();
      expect(() => {
        Request.getShopsDigests(error.message);
      }).toThrow();
      const expected = {
        ...initialStoreValue,
        shopInfoStatus: {
          hasError: true,
          errorMessage: "Request failed with status code 404"
        }
      }
      expect(mobx.toJS(shop)).toEqual(expected);
    });
  });

  describe("getShopInfo()", () => {
    test("getShopInfo should succeed", async () => {
      await shop.getShopInfo(1);
      const expected = {
        ...initialStoreValue,
        selectedShop: {
          address: "テストテストtest1-2-3",
          amenities: [
            {
              id: 42,
              name: "example",
              displayName: "example"
            }
          ],
          capacity: 42,
          hoursFrom: "9:00",
          hoursTo: "13:00",
          id: 1,
          location: {
            latitude: 42.0,
            longitude: 42.0
          },
          name: "SHOPLISTテスト",
          peopleCount: 42,
          photo: {
            large: "shoptestlarge.jpeg",
            thumb: "shoptestthumb.jpeg"
          },
          recentlyPeople: [
            {
              id: 42,
              name: "example",
              displayName: "example",
              bio: "example",
              isFollowing: true,
              lastCheckedInAt: "2015-01-01T12:00:00Z",
              lastCheckedInAtInWords: "example",
              avatar: {
                large: "example",
                thumb: "example"
              }
            }
          ],
          tel: "999-888-7777",
          zipCode: "999-9999"
        },
        shopInfoStatus: {
          hasError: false,
          errorMessage: ""
        }
      }
      expect(mobx.toJS(shop)).toEqual(expected);
    });

    test("getShopInfo should fail", async () => {
      await shop.getShopInfo(1);
      expect(() => {
        Request.getShopsInfo(error.message);
      }).toThrow();
      const expected = {
        ...initialStoreValue,
        shopInfoStatus: {
          hasError: true,
          errorMessage: "Request failed with status code 404"
        }
      }
      expect(mobx.toJS(shop)).toEqual(expected);
    });
  });

  describe("getCheckInUsers()", () => {
    test("getCheckInUsers should succeed", async () => {
      const auth = "Bearer checkInUsers";
      const shopId = 3;
      await shop.getCheckInUsers(auth, shopId);
      const expected = {
        ...initialStoreValue,
        checkInUsers: {
          shop: {
            id: 3,
            name: "example_3"
          },
          checkInUsers: [
            {
              id: 42,
              name: "example",
              displayName: "example",
              bio: "example",
              isFollowing: true,
              lastCheckedInAt: "2015-01-01T12:00:00Z",
              lastCheckedInAtInWords: "example",
              avatar: {
                large: "example",
                thumb: "example"
              }
            }
          ]
        },
        shopInfoStatus: {
          hasError: false,
          errorMessage: ""
        }
      }
      expect(mobx.toJS(shop)).toEqual(expected);
    });

    test("getCheckInUsers should fail", async () => {
      const auth = "Bearer checkInUsers";
      const shopId = 3;
      await shop.getCheckInUsers(auth, shopId);
      expect(() => {
        Request.getShopCheckInUsers(error.message);
      }).toThrow();
      const expected = {
        ...initialStoreValue,
        shopInfoStatus: {
          hasError: true,
          errorMessage: "Request failed with status code 404"
        }
      }
      expect(mobx.toJS(shop)).toEqual(expected);
    });
  });

  describe("getShopListAroundCurrentLocation()", () => {
    test("getShopListAroundCurrentLocation should succeed", async () => {
      const position = {
        latitude: 100,
        longitude: 100
      };
      await shop.getShopListAroundCurrentLocation(position);
      const expected = {
        ...initialStoreValue,
        shopList: [
          {
            id: 4,
            name: "example_4",
            aliasName: "example_4",
            zipCode: "444-4444",
            address: "ショップリスト4-4",
            tel: "444-4444",
            photo: {
              large: "shoplistlarge_4.jpeg",
              thumb: "shoplistthumb_4.jpeg"
            },
            hoursFrom: "6:00",
            hoursTo: "18:00",
            location: {
              latitude: 100,
              longitude: 100
            },
            capacity: 4,
            peopleCount: 4,
            recentlyPeople: [
              {
                id: 4,
                name: "people_4",
                displayName: "people_4",
                bio: "people_4",
                isFollowing: true,
                lastCheckedInAt: "2015-04-01T12:00:00Z",
                lastCheckedInAtInWords: "example_4",
                avatar: {
                  large: "peoplelarge_4.jpeg",
                  thumb: "peoplethumb_4.jpeg"
                }
              }
            ],
            amenities: [
              {
                id: 1,
                name: "amenity_4",
                displayName: "amenity_4"
              }
            ]
          },
          {
            id: 5,
            name: "example_5",
            aliasName: "example_5",
            zipCode: "555-5555",
            address: "ショップリスト5-5",
            tel: "555-555-5555",
            photo: {
              large: "shoplistlarge_5.jpeg",
              thumb: "shoplistthumb_5.jpeg"
            },
            hoursFrom: "5:00",
            hoursTo: "22:00",
            location: {
              latitude: 100.5,
              longitude: 100.5
            },
            capacity: 5,
            peopleCount: 5,
            recentlyPeople: [
              {
                id: 5,
                name: "people_5",
                displayName: "people_5",
                bio: "people_5",
                isFollowing: false,
                lastCheckedInAt: "2018-05-01T12:00:00Z",
                lastCheckedInAtInWords: "example_5",
                avatar: {
                  large: "peoplelarge_5.jpeg",
                  thumb: "peoplethumb_5.jpeg"
                }
              }
            ],
            amenities: [
              {
                id: 5,
                name: "amenity_5",
                displayName: "amenity_5"
              }
            ]
          }
        ],
        shopInfoStatus: {
          hasError: false,
          errorMessage: ""
        }
      };
      expect(mobx.toJS(shop)).toEqual(expected);
    });

    test("getShopListAroundCurrentLocation should fail", async () => {
      const position = {
        latitude: 100,
        longitude: 100
      };
      await shop.getShopListAroundCurrentLocation(position);
      expect(() => {
        Request.getShopsDigests(error.message);
      }).toThrow();
      const expected = {
        ...initialStoreValue,
        shopInfoStatus: {
          hasError: true,
          errorMessage: "Request failed with status code 404"
        }
      }
      expect(mobx.toJS(shop)).toEqual(expected);
    });
  });

});
