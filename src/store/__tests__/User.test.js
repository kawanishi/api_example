import * as mobx from "mobx";
import User from "../User";
import * as Request from "../../api/Request";
import { mock } from "../../api/__mocks__/Client";

jest.mock("../../api/Client");

describe("User", () => {
  let user;
  const initialStoreValue = {
    authorization: "",
    me: {
      name: "",
      realWorldName: "",
      displayName: "",
      email: "",
      tel: "",
      birthday: "",
      address: {
        zipCode: "",
        prefecture: "",
        city: "",
        address: ""
      },
      avatar: {
        large: "",
        thumb: ""
      },
      allowPushNotification: false,
      mainShop: {
        id: 0,
        name: ""
      },
      mainRole: {
        id: 0,
        name: "",
        alias: ""
      },
      bio: "",
      abilities: []
    },
    loginStatus: {
      isSignedIn: false,
      hasError: false,
      errorMessage: ""
    },
    loginUser: {
      email: "",
      password: ""
    },
    editStatus: {
      isEdited: false,
      hasError: false,
      errorMessage: ""
    },
    roleList: [],
    abilityList: []
  };

  beforeEach(() => {
    user = new User();
    jest.clearAllMocks();
    jest.resetAllMocks();
  });

  describe("mobx of when in constructor()", () => {
    test("getInitialData() should run", () => {
      user.getInitialData = jest.fn();
      user.authorization = "Bearer testSucceed";
      user.loginStatus.isSignedIn = true;
      expect(user.getInitialData).toHaveBeenCalledTimes(1);
    });
    test("getInitialData() should not run", () => {
      user.getInitialData = jest.fn();
      user.authorization = "Bearer testFail";
      user.loginStatus.isSignedIn = false;
      expect(user.getInitialData).toHaveBeenCalledTimes(0);
    });
  });

  describe("mobx of reaction in constructor()", () => {
    test("syncUserInfo() should run", () => {
      user.syncUserInfo = jest.fn();
      user.authorization = "Bearer testSucceed";
      user.editStatus.isEdited = true;
      expect(user.syncUserInfo).toHaveBeenCalledTimes(1);
      user.editStatus.isEdited = false;
      expect(user.syncUserInfo).toHaveBeenCalledTimes(1);
      user.authorization = "Bearer updateSucceed";
      user.editStatus.isEdited = true;
      expect(user.syncUserInfo).toHaveBeenCalledTimes(2);
    });

    test("syncUserInfo() should not run", () => {
      user.syncUserInfo = jest.fn();
      user.authorization = "Bearer testFail";
      user.editStatus.isEdited = false;
      expect(user.syncUserInfo).toHaveBeenCalledTimes(0);
    });
  });

  describe("authorization", () => {
    it("should be initial value", () => {
      expect(mobx.toJS(user.authorization)).toEqual(initialStoreValue.authorization);
    });
  });

  describe("loginUser", () => {
    it("should be initial value", () => {
      expect(mobx.toJS(user.loginUser)).toEqual(initialStoreValue.loginUser);
    });
  });

  describe("me", () => {
    it("should be initial value", () => {
      expect(mobx.toJS(user.me)).toEqual(initialStoreValue.me);
    });
  });

  describe("loginStatus", () => {
    it("should be initial value", () => {
      expect(mobx.toJS(user.loginStatus)).toEqual(
        initialStoreValue.loginStatus
      );
    });
  });

  describe("editStatus", () => {
    it("should be initial value", () => {
      expect(mobx.toJS(user.editStatus)).toEqual(initialStoreValue.editStatus);
    });
  });

  describe("roleList", () => {
    it("should be initial value", () => {
      expect(mobx.toJS(user.roleList)).toEqual([]);
    });
  });

  describe("abilityList", () => {
    it("should be initial value", () => {
      expect(mobx.toJS(user.abilityList)).toEqual([]);
    });
  });

  describe("login()", () => {
    test("valid email and password", async () => {
      user.loginUser.email = "testSucceed";
      user.loginUser.password = "testSucceed";
      await user.login();
      const expectedLoginStatus = {
        isSignedIn: true,
        hasError: false,
        errorMessage: ""
      };
      expect(mobx.toJS(user.loginUser.email)).toBe("testSucceed");
      expect(mobx.toJS(user.loginUser.password)).toBe("testSucceed");
      expect(mobx.toJS(user.authorization)).toBe("Bearer testSucceed");
      expect(mobx.toJS(user.loginStatus)).toEqual(expectedLoginStatus);
    });

    test("invalid email and password", async () => {
      user.loginUser.email = "testFail";
      user.loginUser.password = "testFail";
      await user.login();
      const expectedLoginStatus = {
        isSignedIn: false,
        hasError: true,
        errorMessage: "会員情報が存在しません"
      };
      expect(() => {
        Request.postSignIn(error.response.data.message);
      }).toThrow();
      expect(mobx.toJS(user.loginUser.email)).toBe("testFail");
      expect(mobx.toJS(user.loginUser.password)).toBe("testFail");
      expect(mobx.toJS(user.authorization)).toBe("");
      expect(mobx.toJS(user.loginStatus)).toEqual(expectedLoginStatus);
    });
  });

  describe("getInitialData()", () => {
    describe("getInitialData should succeed", () => {
      it("should succeed", async () => {
        user.authorization = "Bearer testSucceed";
        await user.getInitialData();
        const expectedMe = {
          name: "testName",
          realWorldName: "testRealWorldName",
          displayName: "testDisplayName",
          email: "test@test",
          tel: "0009990000",
          birthday: "2000-01-01",
          address: {
            zipCode: "111-0000",
            prefecture: "テスト県",
            city: "テスト市",
            address: "テストアドレス1-1"
          },
          avatar: {
            large: "testLarge.jpeg",
            thumb: "testThumb.jpeg"
          },
          allowPushNotification: true,
          mainShop: {
            id: 1,
            name: "TEST店"
          },
          mainRole: {
            id: 2,
            name: "TESTCREW",
            alias: "testing"
          },
          bio: "テストしています。",
          abilities: [{ id: 5, name: "example" }]
        };
        const expectedLoginStatus = {
          isSignedIn: false,
          hasError: false,
          errorMessage: ""
        };
        expect(mobx.toJS(user.me)).toEqual(expectedMe);
        expect(mobx.toJS(user.loginStatus)).toEqual(expectedLoginStatus);
      });
    });

    describe("getInitialData should fail", () => {
      it("should fail when requesting fetch profile", async () => {
        user.authorization = "Bearer testFail";
        await user.getInitialData();
        expect(() => {
          Request.getMeInfo(error.message);
        }).toThrow();
        const expectedLoginStatus = {
          isSignedIn: false,
          hasError: true,
          errorMessage: "Network Error"
        };
        expect(mobx.toJS(user.me)).toEqual(initialStoreValue.me);
        expect(mobx.toJS(user.loginStatus)).toEqual(expectedLoginStatus);
      });
    });
  });

  describe("updateUserInfo()", () => {
    const data = {
      display_name: "updateDisplayName",
      tel: "1119991111",
      birthday: "2010-01-01",
      address: {
        zip_code: "111-1111",
        prefecture: "更新県",
        city: "更新市",
        address: "テスト更新1-1"
      },
      allow_push_notification: false,
      bio: "更新テスト中",
      main_shop_id: 3,
      main_role_id: 6,
      ability_ids: [10, 20]
    };
    const file = new File(["blob:test"], "test.jpeg");

    test("updateUserInfo should succeed", async () => {
      user.authorization = "Bearer updateSucceed";
      await user.updateUserInfo(data, file);
      const expected = {
        isEdited: true,
        hasError: false,
        errorMessage: ""
      };
      expect(mobx.toJS(user.editStatus)).toEqual(expected);
    });

    test("updateUserInfo should fail", async () => {
      user.authorization = "Bearer updateFail";
      await user.updateUserInfo(data, file);
      expect(() => {
        Request.patchMeProfiles(error.response.data.message);
      }).toThrow();
      const expected = {
        isEdited: false,
        hasError: true,
        errorMessage: "Network Error"
      };
      expect(mobx.toJS(user.editStatus)).toEqual(expected);
    })
  });

  describe("syncUserInfo()", () => {
    describe("syncUserInfo should succeed", () => {
      it("should succeed", async () => {
        user.authorization = "Bearer updateSucceed";
        await user.syncUserInfo();
        const expectedMe = {
          name: "testName",
          realWorldName: "testRealWorldName",
          displayName: "updateDisplayName",
          email: "test@test",
          tel: "1119991111",
          birthday: "2010-01-01",
          address: {
            zipCode: "111-1111",
            prefecture: "更新県",
            city: "更新市",
            address: "テスト更新1-1"
          },
          avatar: {
            large: "updateLarge.jpeg",
            thumb: "updateThumb.jpeg"
          },
          allowPushNotification: false,
          mainShop: {
            id: 3,
            name: "更新店"
          },
          mainRole: {
            id: 6,
            name: "UPDATINGCREW",
            alias: "updating"
          },
          bio: "更新テスト中",
          abilities: [
            { id: 10, name: "example_1" },
            { id: 20, name: "example_2" }
          ]
        };
        const expectedEditStatus = {
          isEdited: false,
          hasError: false,
          errorMessage: ""
        };
        expect(mobx.toJS(user.me)).toEqual(expectedMe);
        expect(mobx.toJS(user.editStatus)).toEqual(expectedEditStatus);
      });
    });

    describe("syncUserInfo should fail", () => {
      it("should fail when requesting fetch profile due to token has been expired", async () => {
        user.authorization = "Bearer tokenExpired";
        await user.syncUserInfo();
        expect(() => {
          Request.getMeInfo(error.response.data.error);
        }).toThrow();
        const expectedEditStatus = {
          isEdited: false,
          hasError: true,
          errorMessage: "アカウント登録もしくはログインしてください。"
        };
        expect(mobx.toJS(user.me)).toEqual(initialStoreValue.me);
        expect(mobx.toJS(user.loginStatus.isSignedIn)).toBe(false);
        expect(mobx.toJS(user.editStatus)).toEqual(expectedEditStatus);
      });
    });
  });

  describe("getRoleList()", () => {
    test("getRoleList should succeed", async () => {
      await user.getRoleList();
      const expected = {
        ...initialStoreValue,
        roleList: [
          { id: 1, name: "example_1", alias: "example_1" },
          { id: 2, name: "example_2", alias: "example_2" }
        ]
      }
      expect(mobx.toJS(user)).toEqual(expected);
    });

    test("getRoleList should fail", async () => {
      await user.getRoleList();
      expect(() => {
        Request.getRoles(error.message);
      }).toThrow();
      const expected = {
        ...initialStoreValue,
        editStatus: {
          isEdited: false,
          hasError: true,
          errorMessage: "Request failed with status code 404"
        }
      }
      expect(mobx.toJS(user)).toEqual(expected);
    });
  });

  describe("getAbilityList()", () => {
    test("getAbilityList should succeed", async () => {
      await user.getAbilityList();
      const expected = {
        ...initialStoreValue,
        abilityList: [
          { id: 1, name: "example_1" },
          { id: 2, name: "example_2" }
        ]
      }
      expect(mobx.toJS(user)).toEqual(expected);
    });

    test("getAbilityList should fail", async () => {
      await user.getAbilityList();
      expect(() => {
        Request.getAbilities(error.message);
      }).toThrow();
      const expected = {
        ...initialStoreValue,
        editStatus: {
          isEdited: false,
          hasError: true,
          errorMessage: "Request failed with status code 404"
        }
      }
      expect(mobx.toJS(user)).toEqual(expected);
    });
  });
});
