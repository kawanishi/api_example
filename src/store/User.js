import * as mobx from "mobx";
import * as Request from "../api/Request";

const { observable, action, when, reaction } = mobx;

export default class User {
  constructor() {
    when(
      () => this.loginStatus.isSignedIn,
      () => this.getInitialData()
    );
    reaction(
      () => this.editStatus.isEdited,
      isEdited => { if (isEdited) this.syncUserInfo(); }
    );
  }

  @observable
  loginUser = {
    email: "",
    // email: process.env.LOGIN_EMAIL,
    password: ""
    // password: process.env.LOGIN_PASSWORD
  };

  @observable authorization = "";

  @observable
  me = {
    name: "",
    realWorldName: "",
    displayName: "",
    email: "",
    tel: "",
    birthday: "",
    address: {
      zipCode: "",
      prefecture: "",
      city: "",
      address: ""
    },
    avatar: {
      large: "",
      thumb: ""
    },
    allowPushNotification: false,
    mainShop: {
      id: 0,
      name: ""
    },
    mainRole: {
      id: 0,
      name: "",
      alias: ""
    },
    bio: "",
    abilities: []
  };

  @observable
  loginStatus = {
    isSignedIn: false,
    hasError: false,
    errorMessage: ""
  };

  @observable
  editStatus = {
    isEdited: false,
    hasError: false,
    errorMessage: ""
  };

  @observable roleList = [];
  @observable abilityList = [];

  @action
  async login() {
    try {
      const data = {
        email: this.loginUser.email,
        password: this.loginUser.password
      };
      const res = await Request.postSignIn(data);
      this.authorization = res.headers.authorization;
      this.loginStatus.hasError = false;
      this.loginStatus.isSignedIn = true;
    } catch (error) {
      if (error.response && error.response.data) {
        this.loginStatus.errorMessage = error.response.data.message;
      } else {
        this.loginStatus.errorMessage = error.message;
      }
      this.loginStatus.hasError = true;
    }
  }

  @action
  async getInitialData() {
    try {
      this.me = await Request.getMeInfo(this.authorization);
    } catch (error) {
      this.loginStatus.errorMessage = error.message;
      this.loginStatus.hasError = true;
    }
  }

  @action
  async updateUserInfo(data, file) {
    try {
      if (file.length !== 0) {
        const readFile = new Promise(resolve => {
          const reader = new FileReader();
          reader.onloadend = () => {
            resolve(reader.result);
          };
          reader.readAsDataURL(file);
        });
        await readFile.then(fileData => {
          data["avatar"] = fileData;
        });
      }
      await Request.patchMeProfiles(this.authorization, data);
      this.editStatus.hasError = false;
      this.editStatus.isEdited = true;
    } catch (error) {
      if (error.response && error.response.data) {
        this.editStatus.errorMessage = error.response.data.message;
      } else {
        this.editStatus.errorMessage = error.message;
      }
      this.editStatus.hasError = true;
    }
  }

  @action
  async syncUserInfo() {
    try {
      this.me = await Request.getMeInfo(this.authorization);
    } catch (error) {
      if (error.response && error.response.data) {
        if (error.response.status === 401) {
          this.loginStatus.isSignedIn = false;
        }
        this.editStatus.errorMessage = error.response.data.error;
        this.editStatus.hasError = true;
      } else {
        this.editStatus.errorMessage = error.message;
        this.editStatus.hasError = true;
      }
    }
  }

  @action
  async getRoleList() {
    try {
      const res = await Request.getRoles();
      this.roleList = res.data;
    } catch (error) {
      this.editStatus.errorMessage = error.message;
      this.editStatus.hasError = true;
    }
  }

  @action
  async getAbilityList() {
    try {
      const res = await Request.getAbilities();
      this.abilityList = res.data;
    } catch (error) {
      this.editStatus.errorMessage = error.message;
      this.editStatus.hasError = true;
    }
  }
}
